import 'reflect-metadata';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import { HttpModule } from '@nestjs/axios';
import { TypeGraphQLModule } from 'typegraphql-nestjs';
import { Stage } from './models/stage.model';
import { Todo } from './models/todo.model';
import { StageModule } from './stage/stage.module';
import { TodoModule } from './todo/todo.module';

@Module({
	imports: [
		HttpModule,
		TypeGraphQLModule.forRoot({
			emitSchemaFile: true,
			validate: false,
			playground: true,
			debug: true,
			context: ({ req }) => ({
				headers: req.headers,
			}),
		}),
		ConfigModule.forRoot({
			isGlobal: true,
		}),
		SequelizeModule.forRootAsync({
			imports: [ConfigModule],
			useFactory: (configService: ConfigService) => ({
				dialect: 'mysql',
				host: configService.get('DATABASE_HOST'),
				port: +configService.get('DATABASE_PORT'),
				username: configService.get('DATABASE_USER'),
				password: configService.get('DATABASE_PASSWORD'),
				database: configService.get('DATABASE_NAME'),
				logging: !!process.env.NODE_LOCAL_ENV || false,
				models: [Stage, Todo],
			}),
			inject: [ConfigService],
		}),
		SequelizeModule.forFeature([Stage, Todo]),
		StageModule,
		TodoModule,
	],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}
