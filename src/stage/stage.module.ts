import { Module } from '@nestjs/common';
import { StageService } from './stage.service';
import { StageResolver } from './stage.resolver';
import { SequelizeModule } from '@nestjs/sequelize';
import { Stage } from '../models/stage.model';
import { Todo } from '../models/todo.model';

@Module({
	providers: [StageService, StageResolver],
	imports: [SequelizeModule.forFeature([Stage, Todo])],
})
export class StageModule {}
