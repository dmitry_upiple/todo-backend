import { Field, ObjectType } from 'type-graphql';
import { Stage } from '../models/stage.model';

@ObjectType()
export class StageListResponseDto {
	@Field()
	status!: boolean;

	@Field((returns) => [Stage], { nullable: true })
	response?: Stage[];

	@Field({ nullable: true })
	error?: string;
}

@ObjectType()
export class StageCreateResponseDto {
	@Field()
	status!: boolean;

	@Field((returns) => Stage, { nullable: true })
	response?: Stage;

	@Field({ nullable: true })
	error?: string;
}

@ObjectType()
export class StageEditResponseDto extends StageCreateResponseDto {}

@ObjectType()
export class StageDeleteResponseDto {
	@Field()
	status!: boolean;

	@Field({ nullable: true })
	error?: string;
}
