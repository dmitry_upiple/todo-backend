import { Field, InputType, Int } from 'type-graphql';
import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsPositive, IsString, ValidateIf } from 'class-validator';

@InputType({ description: 'Stage create request DTO.' })
export class StageCreateRequestDto {
	@Field()
	@IsString()
	@IsNotEmpty()
	name!: string;

	@Field({ nullable: true })
	@ValidateIf((o) => 'sortOrder' in o)
	@IsOptional()
	@IsNumber()
	@IsPositive()
	@IsNotEmpty()
	sortOrder?: number;
}

@InputType({ description: 'Stage edit request DTO.' })
export class StageEditRequestDto {
	@Field((returns) => Int)
	@IsNumber()
	@IsNotEmpty()
	@IsPositive()
	id!: number;

	@Field({ nullable: true })
	@ValidateIf((o) => 'sortOrder' in o)
	@IsOptional()
	@IsString()
	@IsNotEmpty()
	name?: string;

	@Field({ nullable: true })
	@ValidateIf((o) => 'sortOrder' in o)
	@IsOptional()
	@IsNumber()
	@IsPositive()
	@IsNotEmpty()
	sortOrder?: number;

	@Field({ nullable: true })
	@ValidateIf((o) => 'status' in o)
	@IsOptional()
	@IsBoolean()
	@IsNotEmpty()
	status?: boolean;
}

@InputType({ description: 'Stage delete request DTO.' })
export class StageDeleteRequestDto {
	@Field((returns) => Int)
	@IsNumber()
	@IsNotEmpty()
	@IsPositive()
	id!: number;
}
