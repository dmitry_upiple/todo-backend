import { Arg, Mutation, Resolver } from 'type-graphql';
import { Query } from 'type-graphql';
import { Injectable } from '@nestjs/common';
import { StageService } from './stage.service';
import {
	StageCreateResponseDto,
	StageDeleteResponseDto,
	StageEditResponseDto,
	StageListResponseDto,
} from './stage.response.dto';
import { StageCreateRequestDto, StageDeleteRequestDto, StageEditRequestDto } from './stage.request.dto';

@Injectable()
@Resolver()
export class StageResolver {
	constructor(private stageService: StageService) {}

	@Query((returns) => StageListResponseDto)
	async stageList(): Promise<StageListResponseDto> {
		return this.stageService.stageList();
	}

	@Mutation(() => StageCreateResponseDto)
	async stageCreate(@Arg('payload') payload: StageCreateRequestDto): Promise<StageCreateResponseDto> {
		return this.stageService.stageCreate(payload);
	}

	@Mutation(() => StageEditResponseDto)
	async stageEdit(@Arg('payload') payload: StageEditRequestDto): Promise<StageEditResponseDto> {
		return this.stageService.stageEdit(payload);
	}

	@Mutation(() => StageDeleteResponseDto)
	async stageDelete(@Arg('payload') payload: StageDeleteRequestDto): Promise<StageDeleteResponseDto> {
		return this.stageService.stageDelete(payload);
	}

	@Mutation(() => Boolean)
	async clearAllInDatabase(): Promise<boolean> {
		return this.stageService.clearAllInDatabase();
	}
}
