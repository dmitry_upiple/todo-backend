import { Injectable } from '@nestjs/common';
import {
	StageCreateResponseDto,
	StageDeleteResponseDto,
	StageEditResponseDto,
	StageListResponseDto,
} from './stage.response.dto';
import { InjectModel } from '@nestjs/sequelize';
import { Stage } from '../models/stage.model';
import { Todo } from '../models/todo.model';
import { StageCreateRequestDto, StageDeleteRequestDto, StageEditRequestDto } from './stage.request.dto';

@Injectable()
export class StageService {
	constructor(@InjectModel(Stage) private stage: typeof Stage, @InjectModel(Todo) private todo: typeof Todo) {}

	/**
	 * List of stages
	 */
	async stageList(): Promise<StageListResponseDto> {
		const stageList = await this.stage.findAll({
			include: [
				{
					model: Todo,
					as: 'todos',
					order: [['sortOrder', 'DESC']],
				},
			],
			order: [['sortOrder', 'ASC']],
		});
		if (!stageList) {
			return {
				status: false,
				response: null,
				error: `Error. Something went wrong.`,
			};
		}
		return {
			status: true,
			response: stageList,
			error: null,
		};
	}

	/**
	 * Create a stage
	 * @param payload
	 */
	async stageCreate(payload: StageCreateRequestDto): Promise<StageCreateResponseDto> {
		const stage = await this.stage.create(<Stage>(<unknown>{
			name: payload.name,
			sortOrder: payload.sortOrder ? payload.sortOrder : 0,
			status: true,
		}));
		return {
			status: !!stage,
			response: stage ? stage : null,
			error: stage ? null : 'Error. Something went wrong.',
		};
	}

	/**
	 * Edit a stage
	 * @param payload
	 */
	async stageEdit(payload: StageEditRequestDto): Promise<StageEditResponseDto> {
		// Find stage first bu id
		const stage = await this.stage.findByPk(payload.id);
		if (!stage) {
			return {
				status: false,
				response: null,
				error: `Stage with id=${payload.id} doesn't exists.`,
			};
		}
		// Update stage
		if (typeof payload.status === 'boolean') {
			stage.status = payload.status;
		}
		if (payload.name) {
			stage.name = payload.name;
		}
		if (payload.sortOrder) {
			stage.sortOrder = payload.sortOrder;
		}
		if (payload.name || payload.sortOrder || typeof payload.status === 'boolean') {
			await stage.save();
		}
		return {
			status: true,
			response: stage,
			error: null,
		};
	}

	/**
	 * Delete a stage
	 * @param payload
	 */
	async stageDelete(payload: StageDeleteRequestDto): Promise<StageDeleteResponseDto> {
		// First try to find stage by id
		const stage = await this.stage.findByPk(payload.id);
		if (!stage) {
			return {
				status: false,
				error: `Stage with id=${payload.id} doesn't exists.`,
			};
		}
		// Delete stage
		await stage.destroy();
		//TODO maybe we need delete all related todos?
		return {
			status: true,
			error: null,
		};
	}

	/**
	 * Simple update all records(NOT PRODUCTION)
	 */
	async clearAllInDatabase(): Promise<boolean> {
		await this.stage.update(
			{
				status: true,
			},
			{
				where: {
					status: false,
				},
			},
		);
		await this.todo.update(
			{
				status: true,
			},
			{
				where: {
					status: false,
				},
			},
		);
		return true;
	}
}
