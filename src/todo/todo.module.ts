import { Module } from '@nestjs/common';
import { TodoService } from './todo.service';
import { TodoResolver } from './todo.resolver';
import { SequelizeModule } from '@nestjs/sequelize';
import { Stage } from '../models/stage.model';
import { Todo } from '../models/todo.model';

@Module({
	providers: [TodoService, TodoResolver],
	imports: [SequelizeModule.forFeature([Stage, Todo])],
})
export class TodoModule {}
