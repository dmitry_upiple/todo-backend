import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Stage } from '../models/stage.model';
import { Todo } from '../models/todo.model';
import {
	TodoCreateResponseDto,
	TodoDeleteResponseDto,
	TodoEditResponseDto,
	TodoListResponseDto,
} from './todo.response.dto';
import { TodoCreateRequestDto, TodoDeleteRequestDto, TodoEditRequestDto } from './todo.request.dto';

@Injectable()
export class TodoService {
	constructor(@InjectModel(Stage) private stage: typeof Stage, @InjectModel(Todo) private todo: typeof Todo) {}

	/**
	 * List of todo_
	 */
	// TODO maybe add filter param for finding only todo from one stage in future?
	async todoList(): Promise<TodoListResponseDto> {
		const todos = await this.todo.findAll({
			include: [
				{
					model: Stage,
					as: 'stage',
				},
			],
			order: [['sortOrder', 'ASC']],
		});
		if (!todos) {
			return {
				status: false,
				response: null,
				error: `Error due getting todo list.`,
			};
		}
		return {
			status: true,
			response: todos,
			error: null,
		};
	}

	/**
	 * Create a todo_
	 * @param payload
	 */
	async todoCreate(payload: TodoCreateRequestDto): Promise<TodoCreateResponseDto> {
		// First check if we already have stage
		const stage = await this.stage.findByPk(payload.stageId);
		if (!stage) {
			return {
				status: false,
				response: null,
				error: `Stage with id=${payload.stageId} doesn't exists.`,
			};
		}
		const todo = await this.todo.create(<Todo>(<unknown>{
			name: payload.name,
			stageId: payload.stageId,
			sortOrder: payload.sortOrder ? payload.sortOrder : 0,
			status: true,
		}));
		return {
			status: !!todo,
			response: todo ? todo : null,
			error: todo ? null : 'Error. Something went wrong.',
		};
	}

	/**
	 * Edit a todo_
	 * @param payload
	 */
	async todoEdit(payload: TodoEditRequestDto): Promise<TodoEditResponseDto> {
		// Find first todo_ by id
		const todo = await this.todo.findByPk(payload.id);
		if (!todo) {
			return {
				status: false,
				response: null,
				error: `Todo with id=${payload.id} doesn't exists.`,
			};
		}
		// Set stage variable for check
		let stage;
		if (typeof payload.status === 'boolean') {
			todo.status = payload.status;
		}
		if (payload.name) {
			todo.name = payload.name;
		}
		if (payload.sortOrder) {
			todo.sortOrder = payload.sortOrder;
		}
		if (payload.stageId) {
			// Check if we have current stage
			stage = await this.stage.findByPk(payload.stageId);
			if (stage) {
				todo.stageId = payload.stageId;
			} // TODO otherwise if we do not have this stage should we throw an Error ?
		}
		if (payload.name || stage || payload.sortOrder || typeof payload.status === 'boolean') {
			await todo.save();
		}
		// Check if all from todo is done then we set stage is done
		const check = await this.todo.findOne({
			where: {
				stageId: todo.stageId,
				status: true,
			},
		});
		if (!check) {
			const _stage = await this.stage.findByPk(todo.stageId);
			_stage.status = false;
			await _stage.save();
		} else {
			const _stage = await this.stage.findByPk(todo.stageId);
			_stage.status = true;
			await _stage.save();
		}
		return {
			status: true,
			response: todo,
			error: null,
		};
	}

	/**
	 * Delete a todo_
	 * @param payload
	 */
	async todoDelete(payload: TodoDeleteRequestDto): Promise<TodoDeleteResponseDto> {
		// Find first todo_ by id
		const todo = await this.todo.findByPk(payload.id);
		if (!todo) {
			return {
				status: false,
				error: `Todo with id=${payload.id} doesn't exists.`,
			};
		}
		// Delete todo_
		await todo.destroy();
		return {
			status: true,
			error: null,
		};
	}
}
