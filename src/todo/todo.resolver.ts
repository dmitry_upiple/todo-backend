import { Arg, Mutation, Resolver } from 'type-graphql';
import { Query } from 'type-graphql';
import { Injectable } from '@nestjs/common';
import { TodoService } from './todo.service';
import {
	TodoCreateResponseDto,
	TodoDeleteResponseDto,
	TodoEditResponseDto,
	TodoListResponseDto,
} from './todo.response.dto';
import { TodoCreateRequestDto, TodoDeleteRequestDto, TodoEditRequestDto } from './todo.request.dto';

@Injectable()
@Resolver()
export class TodoResolver {
	constructor(private todoService: TodoService) {}

	@Query((returns) => TodoListResponseDto)
	async todoList(): Promise<TodoListResponseDto> {
		return this.todoService.todoList();
	}

	@Mutation(() => TodoCreateResponseDto)
	async todoCreate(@Arg('payload') payload: TodoCreateRequestDto): Promise<TodoCreateResponseDto> {
		return this.todoService.todoCreate(payload);
	}

	@Mutation(() => TodoEditResponseDto)
	async todoEdit(@Arg('payload') payload: TodoEditRequestDto): Promise<TodoEditResponseDto> {
		return this.todoService.todoEdit(payload);
	}

	@Mutation(() => TodoDeleteResponseDto)
	async todoDelete(@Arg('payload') payload: TodoDeleteRequestDto): Promise<TodoDeleteResponseDto> {
		return this.todoService.todoDelete(payload);
	}
}
