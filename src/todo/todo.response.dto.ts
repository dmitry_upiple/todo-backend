import { Field, ObjectType } from 'type-graphql';
import { Todo } from '../models/todo.model';

@ObjectType()
export class TodoListResponseDto {
	@Field()
	status!: boolean;

	@Field((returns) => [Todo], { nullable: true })
	response?: Todo[];

	@Field({ nullable: true })
	error?: string;
}

@ObjectType()
export class TodoCreateResponseDto {
	@Field()
	status!: boolean;

	@Field((returns) => Todo, { nullable: true })
	response?: Todo;

	@Field({ nullable: true })
	error?: string;
}

@ObjectType()
export class TodoEditResponseDto extends TodoCreateResponseDto {}

@ObjectType()
export class TodoDeleteResponseDto {
	@Field()
	status!: boolean;

	@Field({ nullable: true })
	error?: string;
}
