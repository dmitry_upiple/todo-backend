import { Field, InputType, Int } from 'type-graphql';
import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsPositive, IsString, ValidateIf } from 'class-validator';

@InputType({ description: 'Todo create request DTO.' })
export class TodoCreateRequestDto {
	@Field()
	@IsString()
	@IsNotEmpty()
	name!: string;

	@Field((returns) => Int)
	@IsNumber()
	@IsPositive()
	@IsNotEmpty()
	stageId!: number;

	@Field({ nullable: true })
	@ValidateIf((o) => 'sortOrder' in o)
	@IsOptional()
	@IsNumber()
	@IsPositive()
	@IsNotEmpty()
	sortOrder?: number;
}

@InputType({ description: 'Todo edit request DTO.' })
export class TodoEditRequestDto {
	@Field((returns) => Int)
	@IsNumber()
	@IsNotEmpty()
	@IsPositive()
	id!: number;

	@Field({ nullable: true })
	@ValidateIf((o) => 'sortOrder' in o)
	@IsOptional()
	@IsString()
	@IsNotEmpty()
	name?: string;

	@Field((returns) => Int, { nullable: true })
	@ValidateIf((o) => 'stageId' in o)
	@IsNumber()
	@IsOptional()
	@IsPositive()
	@IsNotEmpty()
	stageId?: number;

	@Field({ nullable: true })
	@ValidateIf((o) => 'sortOrder' in o)
	@IsOptional()
	@IsNumber()
	@IsPositive()
	@IsNotEmpty()
	sortOrder?: number;

	@Field({ nullable: true })
	@ValidateIf((o) => 'status' in o)
	@IsOptional()
	@IsBoolean()
	@IsNotEmpty()
	status?: boolean;
}

@InputType({ description: 'Todo delete request DTO.' })
export class TodoDeleteRequestDto {
	@Field((returns) => Int)
	@IsNumber()
	@IsNotEmpty()
	@IsPositive()
	id!: number;
}
