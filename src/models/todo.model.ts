import { AllowNull, BelongsTo, Column, Default, ForeignKey, Model, Sequelize, Table } from 'sequelize-typescript';
import { Field, Int, ObjectType } from 'type-graphql';
import { Stage } from './stage.model';

@ObjectType()
@Table({
	tableName: 'todo',
})
export class Todo extends Model<Todo> {
	@AllowNull(false)
	@Column({ primaryKey: true, autoIncrement: true, field: 'id' })
	@Field((returns) => Int)
	id!: number;

	@ForeignKey(() => Stage)
	@AllowNull(false)
	@Column({ field: 'stageId' })
	@Field((returns) => Int)
	stageId!: number;

	@AllowNull(false)
	@Column({ field: 'name' })
	@Field()
	name!: string;

	@Default(true)
	@Column({ field: 'status' })
	@Field()
	status!: boolean;

	@Default(true)
	@Column({ field: 'sortOrder' })
	@Field((returns) => Int)
	sortOrder!: number;

	@Default(Sequelize.literal('CURRENT_TIMESTAMP()'))
	@AllowNull(false)
	@Column({ field: 'createdAt' })
	@Field()
	createdAt!: Date;

	@Default(Sequelize.literal('CURRENT_TIMESTAMP()'))
	@AllowNull(false)
	@Column({ field: 'updatedAt' })
	@Field()
	updatedAt!: Date;

	@BelongsTo(() => Stage)
	@Field((returnts) => Stage, { nullable: true })
	stage: Stage;
}
