import { AllowNull, Column, Default, HasMany, Model, Sequelize, Table } from 'sequelize-typescript';
import { Field, ObjectType } from 'type-graphql';
import { Todo } from './todo.model';

@ObjectType()
@Table({
	tableName: 'stage',
})
export class Stage extends Model<Stage> {
	@AllowNull(false)
	@Column({ primaryKey: true, autoIncrement: true, field: 'id' })
	@Field()
	id!: number;

	@AllowNull(false)
	@Column({ field: 'name' })
	@Field()
	name!: string;

	@Default(true)
	@Column({ field: 'status' })
	@Field()
	status!: boolean;

	@Default(true)
	@Column({ field: 'sortOrder' })
	@Field({ nullable: true })
	sortOrder!: number;

	@Default(Sequelize.literal('CURRENT_TIMESTAMP()'))
	@AllowNull(false)
	@Column({ field: 'createdAt' })
	@Field()
	createdAt!: Date;

	@Default(Sequelize.literal('CURRENT_TIMESTAMP()'))
	@AllowNull(false)
	@Column({ field: 'updatedAt' })
	@Field()
	updatedAt!: Date;

	@HasMany(() => Todo)
	@Field((returns) => [Todo], { nullable: true })
	todos: Todo[];
}
