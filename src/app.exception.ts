import { Catch, ArgumentsHost, HttpException, HttpStatus, ContextType } from '@nestjs/common';
import { GqlExceptionFilter } from '@nestjs/graphql';

@Catch()
export class AllExceptionsFilter implements GqlExceptionFilter {
	catch(exception: unknown, host: ArgumentsHost) {
		switch (host.getType() as ContextType | 'graphql') {
			case 'graphql':
				return exception;

			default:
				const ctx = host.switchToHttp();
				const response = ctx.getResponse();

				const status =
					exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

				console.error('Catch Error', exception);
				response.status(status).json({
					status: false,
					error: exception instanceof HttpException ? exception.message : exception.toString(),
					message: exception['response']?.message,
				});
				break;
		}
	}
}
